package com.vaecon.rafflr;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RunWith(JUnit4.class)
public class UserRepositoryTests {
    UserRepository userRepository = new UserRepository(new UserFileReader());
    @Test
    public void getRandomGetsRandom(){
        User randomUser = userRepository.getRandom();
        Assert.assertTrue(randomUser.getUserName().startsWith("Vaecon"));
    }

    @Test
    public void getRandom10(){

        List<User> resultList = new ArrayList<User>();
        for(int i = 0; i < 10; i++){
            User randomUser = userRepository.getRandom();
            resultList.add(randomUser);
        }
        Set testSet = new HashSet<>(resultList);
        Assert.assertTrue(testSet.size() > 1);
    }
}
