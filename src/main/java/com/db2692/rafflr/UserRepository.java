package com.vaecon.rafflr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Random;

@Repository
public class UserRepository {
    ArrayList<User> userList;
    Random rnJesus = new Random();

    @Autowired
    public UserRepository(UserFileReader userFileReader){
        userList = userFileReader.read();

    }

    public int getListSize(){
        return userList.size();
    }

    public User getRandom(){
        //System.out.println(userList);

        return userList.get(rnJesus.nextInt(userList.size()));

    }
}
