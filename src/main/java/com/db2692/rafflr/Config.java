package com.vaecon.rafflr;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Value("${filePath}")
    String path;

    @Value("${uniqueEntries}")
    boolean uniqueEntries;

    @Value("${winners}")
    int winners;

    @Value("${uniqueWinners}")
    boolean uniqueWinners;



}


