package com.vaecon.rafflr;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

@Component
public class RafflrService {

    private final UserRepository newRaffle;
    private Config config;

    @Autowired
    public RafflrService(UserRepository newRaffle, Config config) {
        this.newRaffle = newRaffle;
        this.config = config;
    }


    @PostConstruct
    public void runRaffle(){

        if(newRaffle.getListSize() < config.winners) {
            throw new RuntimeException("Not enough users to pick a unique number of winners.");
        }

        ArrayList<User> winnerList = new ArrayList<User>();

        while (winnerList.size() < config.winners) {
            User winner = newRaffle.getRandom();
            if (!config.uniqueWinners || !winnerList.contains(winner)) {
                winnerList.add(winner);
            }
        }

        winnerList.stream().forEach(i -> System.out.println(i));

    }

}
