package com.vaecon.rafflr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RafflrApplication {

    public static void main(String[] args) {
        SpringApplication.run(RafflrApplication.class, args);
    }

}
