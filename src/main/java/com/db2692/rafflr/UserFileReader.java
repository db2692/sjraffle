package com.vaecon.rafflr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;


@Component
public class UserFileReader {

    private Config config;

    @Autowired
    public UserFileReader(Config config){

        this.config = config;
    }

    public ArrayList<User> read() {
        ArrayList<User> userList= new ArrayList<User>();

        Path pathToFile = Paths.get(config.path);

        try(BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.UTF_8)){
            String line = br.readLine();
            line = br.readLine();

            while(line != null){
                String[] userData = line.split(",");
                if(userData.length > 2) {
                    User newUser = new User(userData[0], userData[2]);

                    if(config.uniqueEntries) {
                        if (userList.stream().noneMatch(u -> u.getEmail().equalsIgnoreCase(userData[2]))) {
                            userList.add(newUser);
                        }
                    }
                    else{
                        userList.add(newUser);
                    }

                }
                line = br.readLine();
            }


        } catch (IOException ioe){
            ioe.printStackTrace();
        }


        return userList;
    }
}

//